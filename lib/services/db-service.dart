import 'dart:convert';
import 'package:http/http.dart' as http;

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:jchproto4/services/local-db.dart';
import 'package:sembast/sembast.dart';

class DBService {

  static const String IMAGE_STORE = 'hunting_images';
  final _imageStore = stringMapStoreFactory.store(IMAGE_STORE);
  Future<Database> get _db async => await AppDatabase.instance.database;
  final Firestore _firestore = Firestore.instance;
  final FirebaseStorage _storage = FirebaseStorage(storageBucket:'gs://hunt-planning-app.appspot.com');

  Stream getHuntingImageNames(String huntDay){

    return _firestore.collection('huntingImages').snapshots();
  }

  // Future getImageListFromCloudStorage(String huntDay){
  //   final int maxSize = 2048 * 2048;
  //   final String imagePath = 'images/huntingImages/**';
  //   print('getting images from $imagePath');
  //   //return _storage.ref().child(imagePath).getData(maxSize);
  //   return _storage.ref().child('images').;
  // }

  Future<String> getHuntingImageUrl(String imagePath) async{
    //print('service called');
    String url = await _storage.ref().child(imagePath).getDownloadURL();
    print('url is $url');
    return url;
  }

  Future<String> getDownloadUrl(StorageReference imageRef) async {
    final String url = await imageRef.getDownloadURL();
    return url;
  }

  Future<DocumentReference> getUserMeta(String userID) async{
    return _firestore.collection('users').document(userID);
  }

  Future getCompany(companyID) async{
    //print('fetching company');
    var company =  await _firestore.collection('companies').document(companyID).get();
    //print(company.data);
    return Future.value(company.data);
  }

  Stream getGuides(){
    return _firestore.collection('users').where('userType', isEqualTo: 1).snapshots();
    //return _firestore.collection('users').snapshots();
  }

  Stream getFields(){
    return _firestore.collection('fields').snapshots();
  }

  Stream getHuntingPlans(String dayID) {
    print('getting data for $dayID');
    //if(dayID != null){
      return _firestore.collection('huntingPlans').document(dayID).snapshots();
      
      //   return Future.value(null);
      // return Future.value(doc);
    //}
    //return Future.value(null);
  }

  Future updateHuntPlans(String huntDay, Map plans){

    return _firestore.collection('huntingPlans').document(huntDay).updateData(plans);
  }

  Future assignGuide(Map doc, dayID) async{
    print('doc to update $doc');
    print('id $dayID');
    _firestore.collection('huntingPlans').document(dayID).updateData(doc).then((value){
      print('success updated');
    });
  }

  void uploadAndStoreImage(String filePath, String filename, file, plans) async {
    Map<String, dynamic> imgObj = {
      'data': base64Encode(file.readAsBytesSync()),
      'uploaded': false
    };

    
    //store to cloud
    _storage.ref().child(filePath).putFile(file).events.listen((uploadTask) async { 
      if(uploadTask.type == StorageTaskEventType.success){
        //todo save ref to firestore
        String downloadUrl = await uploadTask.snapshot.ref.getDownloadURL();
        Map<String, dynamic> imgCloudObj = {
          'imageName': filename,
          'imageUrl': downloadUrl,
          'thumbUrl': ''
        };
        Map<String, dynamic> planToUpdate = {
          'plans': plans
        };
        planToUpdate['plans'][0]['pictures'].add(imgCloudObj);
        await updateHuntPlans('JCH06062020', planToUpdate);
        //save to local
        imgObj['uploaded'] = true;
        insertToImageStore(filePath, imgObj);
      }else if(uploadTask.type == StorageTaskEventType.failure){
        //save to local with flag
        insertToImageStore(filePath, imgObj);
      }
    }); 
  }

  Stream getImageFromStoreAndCloud(String imagePath, String imageUrl) async* {
    //todo some images might be in the local db without upload must list them too 
    yield CircularProgressIndicator();
    final storeImage = await getfromImageStore(imagePath);
    if(storeImage != null){
      print('found image in store');
      yield Image.memory(base64Decode(storeImage['data']));
    }else{
      print('no store image found');
      http.Response response = await http.get(imageUrl);
      print('got image from network');
      //store this image in store
      Map<String, dynamic> imgObj = {
      'data': base64.encode(response.bodyBytes),
      'uploaded': true
      };
      await insertToImageStore(imagePath, imgObj);
      yield Image.memory(response.bodyBytes);
    }
  }

  Future insertToImageStore(String key, Map<String, dynamic> imgObj) async {
    return _imageStore.record(key).put(await _db, imgObj);
    
  }

  Future getfromImageStore(String imagePath) async{
    return _imageStore.record(imagePath).get(await _db);
  }
}