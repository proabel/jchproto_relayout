import 'dart:async';

import 'package:path_provider/path_provider.dart';
import 'package:sembast/sembast.dart';
import 'package:sembast/sembast_io.dart';


class AppDatabase {
  static final AppDatabase _singleton = AppDatabase._();

  static AppDatabase get instance => _singleton;

  Completer<Database> _dbCompleter;

  AppDatabase._();

  Future<Database> get database async {
    if(_dbCompleter == null) { 
      _dbCompleter = Completer();
      _openDatabase();
    }
    return _dbCompleter.future;
  }

  Future _openDatabase() async {
    final appDocumentDir = await getApplicationDocumentsDirectory();
    final dbPath = '${appDocumentDir.path}jch_app.db';
    final database = await databaseFactoryIo.openDatabase(dbPath);
    _dbCompleter.complete(database);
  }
}