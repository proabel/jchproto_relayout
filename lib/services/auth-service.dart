import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

class AuthService {
  final Firestore _firestore = Firestore.instance;
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

  Future signInWithCredentials({String email, String password}) async{
    var authUser = await _firebaseAuth.signInWithEmailAndPassword(email: email, password: password).catchError((e){
      return e;
    });
    var user = await _firestore.collection('users').where('userAuthID', isEqualTo: authUser.user.uid).getDocuments();
    return user.documents.first.data;
  }

  Future<bool> isSignedIn() async{
    final currentUser = await _firebaseAuth.currentUser();
    return currentUser != null;
  }

  Future getCurrentUser() async{
    print('fetching current use');
    FirebaseUser authUser = await _firebaseAuth.currentUser();
    var user = await _firestore.collection('users').where('userAuthID', isEqualTo: authUser.uid).getDocuments();
    return user.documents.first.data;
  }

  Future<String> getUserID() async{
    FirebaseUser authUser = await _firebaseAuth.currentUser();
    return authUser.uid;
  }

  Future<void> signOut() async{
    return _firebaseAuth.signOut();
  }
}