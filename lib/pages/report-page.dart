
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:jchproto4/services/auth-service.dart';
import 'package:provider/provider.dart';

import './../providers/app-state-provider.dart';
import './../utils/enums.dart';
import './../utils/screen-size-util.dart';
import '../services/db-service.dart';


const almostWhite = Color(0xFFFAFAFA);
class ReportPage extends StatefulWidget {
  ReportPage({Key key}) : super(key: key);

  @override
  _ReportPageState createState() => _ReportPageState();
}

class _ReportPageState extends State<ReportPage> {
  final _auth = AuthService();
  final _dbService = DBService();
  String imageToPreview;
  String userID;

  @override
  void initState() {
    super.initState();
    getUserID();
  }

  void getUserID() async {
    final String id = await _auth.getUserID();
    setState(() {
      userID = id;
    });
  }

  @override
  Widget build(BuildContext context) {
    ScreenSize().init(context);
    return Scaffold(
      backgroundColor: const Color(0xFFEEE5CC),
      appBar: _buildAppBar(context),
      body: _buildReportScreenLayout(context)
    );
  }

  Widget _buildAppBar(context){
    
    return AppBar(
      actions: <Widget>[
        Padding(
          padding: const EdgeInsets.only(right:20.0),
          child: IconButton(icon: Icon(Icons.power_settings_new, size: 32, color: almostWhite), onPressed: (){
            //_auth.signOut();
            Navigator.pushReplacementNamed(context, '/');
          }),
        )
      ],
      centerTitle: false,
      title: Padding(
        padding: const EdgeInsets.only(left:20.0),
        child: const Text('Joshua Creek Hunt Plan', style: TextStyle(fontSize: 32, color: almostWhite), textAlign: TextAlign.start,),
      )
    );
  }

  Widget _buildReportScreenLayout(context){
    return Container(
      color: const Color(0xFFEEE5CC),
      child: OrientationBuilder(
        builder: (context, orientation){
          //print('orientation changed');
          if(ScreenSize.screenWidth > 1200){
            
            return _buildLargeLayout(context);
          }else if(ScreenSize.screenWidth > 800){
            return _buildSmallLayout(context);
          }else{
            //return _buildSmallLayout(context);
            print('building small layout');
            return _buildSmallLayout(context);
          }
        }
      ),
    );
  }  

  Widget _buildLargeLayout(context){
    return Stack(
      children: [
        _buildLargeMainPanel(context),

        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            _buildActionsPanel(context),
            _buildDatePanel(context),
          ],
        ),
        _buildImagePreview(),
      ],
    );
  }

  Widget _buildSmallLayout(context){
    return Stack(
      children: [
        _buildSmallMainPanel(context),
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _buildActionsPanel(context),
            SizedBox(height:20.0),
            _buildDatePanel(context),
          ],
        ),
        _buildImagePreview(),
      ],
    );
  }

  Widget _buildActionsPanel(context){
    return FittedBox(
      child: Container(
        color:const Color(0xFFEEE5CC),
        padding: const EdgeInsets.all(10.0),
        child: Card(
          color: almostWhite,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(28.0)
          ),
          elevation: 5.0,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              SizedBox(width:15.0),
              IconButton(
                icon: Icon(Icons.send, color: Theme.of(context).primaryColor,), 
                onPressed: null,
                iconSize: 28.0,
                padding: EdgeInsets.symmetric(vertical: 13.0),
              ),
              SizedBox(width: 5.0,),
              IconButton(
                icon: Icon(Icons.lock_open, color: Theme.of(context).primaryColor,), 
                onPressed: null,
                iconSize: 28.0,
              ),
              SizedBox(width: 5.0,),
              IconButton(
                icon: Icon(Icons.edit, color: Theme.of(context).primaryColor,), 
                onPressed: null,
                iconSize: 28.0,
              ),
              SizedBox(width: 5.0,),
              IconButton(
                icon: Icon(Icons.sync, color: Theme.of(context).primaryColor,), 
                onPressed: null,
                iconSize: 28.0,
              ),
              SizedBox(width: 5.0,),
              IconButton(
                icon: (FaIcon(FontAwesomeIcons.userAlt, color: Theme.of(context).primaryColor,)), 
                onPressed: (){
                  Navigator.pushReplacementNamed(context, '/lead');
                },
                iconSize: 28.0,
              ),
              SizedBox(width:15.0),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildDatePanel(context){
    DateFormat selectedDateFormat = new DateFormat('yMMMd');
    return Consumer<AppState>(
      builder: (context, appState, child){
        DateTime selectedDate = appState.selectedDate;
        return FittedBox(
          child: Container(
            color:const Color(0xFFEEE5CC),
            padding: EdgeInsets.all(20.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                _buildRainSelector(appState),
                SizedBox(width:20),
                _buildAMPMSelector(appState),
                //isRaining ? FaIcon(FontAwesomeIcons.cloudShowersHeavy, size: 32, color: Colors.lightBlue,) : FaIcon(FontAwesomeIcons.cloudSun, size: 32, color: Colors.deepOrange,),
                SizedBox(width:20),
                GestureDetector(
                  onTap: (){
                    _selectDate(context, appState);
                    
                  },
                  child:Text('${selectedDateFormat.format(selectedDate)}', style: TextStyle(fontSize:32, color: Colors.black54,),),
                  //child: Text('DATe')
                ),
                SizedBox(width: 20),
                _buildNetworkStatus(),
                SizedBox(width:10),
              ],
            ),
          )
        );
      }
    );
  }

  Widget _buildAMPMSelector(appState){
    return GestureDetector(
      onTap: (){
        //setState(() {
          appState.selectedHour == Hour.am ? appState.setHour(Hour.pm) : appState.setHour(Hour.am);
        //});
      },
      child: appState.selectedHour == Hour.am ? Text('AM', style: TextStyle(fontSize:32, color: Colors.black54,)) : Text('PM', style: TextStyle(fontSize:32, color: Colors.black54,))
      //child: Text('PM', style: TextStyle(fontSize:32, color: Colors.black54,))
    );
  }
  Widget _buildRainSelector(appState){
    bool isRaining = appState.isRaining;
    return GestureDetector(
      onTap:(){
        appState.setIsRaining(!isRaining);
      },
      child: appState.isRaining ? FaIcon(FontAwesomeIcons.cloudShowersHeavy, size: 32, color: Colors.lightBlue,) : FaIcon(FontAwesomeIcons.cloudSun, size: 32, color: Colors.deepOrange,),
      //child: FaIcon(FontAwesomeIcons.cloudSun, size: 32, color: Colors.deepOrange,),

    );
  }

  Future<Null> _selectDate(BuildContext context, appState) async {
    DateTime selectedDate = appState.selectedDate;
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: appState.selectedDate,
        firstDate: DateTime(2019),
        lastDate: DateTime(2021));

    if (picked != null && picked != selectedDate){
      appState.setSelectedDate(picked);
      print('set date success');
    }
      //setState(() {
        //selectedDate = picked;
        //huntDay = company['meta']['shortName'] + shortDate.format(picked).toString();
        //_getHuntPLans();
      //});
  }

  Widget _buildNetworkStatus(){
    return StreamBuilder(
      stream: Stream.periodic(Duration(seconds: 3), (x) async{
        try{
          final result = await InternetAddress.lookup('google.com');
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            return true;
          }
        }on SocketException catch(_){
          return false;
        }
      }),
      builder: (_, resultFuture){
        return FutureBuilder(
          future: resultFuture.data,
          builder: (_, result){
            if(result.data != null && result.data)
              return Icon(Icons.wifi, size: 36, color: Theme.of(context).primaryColor);
            else
              return Icon(Icons.signal_wifi_off, size: 36, color: Colors.grey);
          }
        );
      }
    );
  }

  Widget _buildLargeMainPanel(context){
    return SingleChildScrollView(
      child: Container(
        color: const Color(0xFFEEE5CC),
        padding: EdgeInsets.only(top: 100.0, left: 20.0 ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
              ConstrainedBox(
                constraints: BoxConstraints(
                  minHeight:100,
                ),
                child: SizedBox.shrink()
              ),
            SizedBox(height:20.0),
            
            _buildImageSlider(context),
          ]      // _buildGuideComments(context),
                  // _buildWaypointsMap(context),
               
        )
      ),
    );
  }

    Widget _buildSmallMainPanel(context){
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.only(top: 100.0, left: 20.0 ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
              ConstrainedBox(
                constraints: BoxConstraints(
                  minHeight:500,
                ),
                child: SizedBox.shrink()
              ),
            SizedBox(height:20.0),
            
             
                  _buildImageSlider(context),
               
          ],
        )
      ),
    );
  }




  String _getPreyNumbers(String preyID, List preys){
    final int index = preys.indexWhere((prey) => prey['preyID'] == preyID);
    final String numbers = preys[index]['numbers'].toString();
    return numbers != null ? numbers : '0'; 
  }

  String _getGuideName(String guideID, guides){
    String name = "No Name";
    guides.forEach((guide) {
      if(guide.documentID == guideID){
        name = guide.data['meta']['firstName'];
      }
    });
    return name;
  }




  Widget _buildImageSlider(context){
    // final List huntImages = [
    //   'https://images.unsplash.com/photo-1568282344004-be2ab24a5c35?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1500&q=80',
    //   'https://images.unsplash.com/photo-1556478094-bf761a15ef40?ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80',
    //   'https://images.unsplash.com/photo-1578274745919-c47d6ada9d37?ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80',
    //   'https://images.unsplash.com/photo-1548171971-b56560f9a7bb?ixlib=rb-1.2.1&auto=format&fit=crop&w=967&q=80',
    //   'https://images.unsplash.com/photo-1521477177889-6420230f42a1?ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80'
    // ];
    List huntImages;

    return Container(
      //color: Color(0xFF202020),
      height: 200,
      padding: EdgeInsets.all(10.0),
      child: Consumer<AppState>(
        builder: (context, appState, child){
          
          return StreamBuilder(
            stream: _dbService.getHuntingPlans(appState.huntDay),
            builder: (_, AsyncSnapshot snapshot){
              //print(snapshot.data['plans']);
              if(snapshot.hasError) return Text('Error : ${snapshot.error}');
              switch (snapshot.connectionState) {
                case ConnectionState.waiting:
                  return Text('loading images');
                  break;
                default:
                  if(snapshot.hasData && snapshot.data.exists){
                    //print(snapshot.data['plans'][0]);
                    huntImages = snapshot.data['plans'][0]['pictures'].toList();
                    //print('hunt images $huntImages');
                    //return Text('got Images');
                    return SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: Container(
                          child: ListView.builder(
                            scrollDirection: Axis.horizontal,
                            shrinkWrap: true,
                            itemCount: huntImages.length,
                            itemBuilder: (BuildContext context, index){
                              final imagePath = 'images/${appState.huntDay}/${huntImages[index]['imageName']}';
                              final imageUrl = huntImages[index]['imageUrl'];
                              return StreamBuilder(
                                stream: _dbService.getImageFromStoreAndCloud(imagePath, imageUrl),
                                builder: (_, AsyncSnapshot snapshot){
                                  print(snapshot.toString());
                                  if(snapshot.data != null)
                                    return snapshot.data;
                                  else
                                    return Text('error fetching image');
                                }
                              );
                              // return Container(
                              //   padding: EdgeInsets.all(10.0),
                              //   child: _dbService.getImageFromStoreOrCloud(imagePath, imageUrl)
                              //   //child: Text('Loading Image')
                              //   //child: _buildImageThumb(huntImages[index])
                              //   //child: Image.network(huntImages[index])
                              // );
                          }),
                        ),
                      );
                  }
                  return Text('No images in Server');
                }
          });
        },
      ),
      // child: StreamBuilder(
      //   stream: _dbService.getHuntingImageNames(),
      //   builder: (_, AsyncSnapshot snapshot){
      //     if(snapshot.hasError) return Text('Error : ${snapshot.error}');
      //     switch (snapshot.connectionState) {
      //       case ConnectionState.waiting:
      //         return Text('loading images');
      //         break;
      //       default:
      //         if(snapshot.data.documents != null){
      //           huntImages = snapshot.data.documents.toList();
      //           print('hunt images $huntImages');
      //           // return Text('got Images');
      //           return SingleChildScrollView(
      //               scrollDirection: Axis.horizontal,
      //               child: Container(
      //                 child: ListView.builder(
      //                   scrollDirection: Axis.horizontal,
      //                   shrinkWrap: true,
      //                   itemCount: huntImages.length,
      //                   itemBuilder: (BuildContext context, index){
      //                     return Container(
      //                       padding: EdgeInsets.all(10.0),
      //                       child: _buildImageThumb(huntImages[index])
      //                       //child: Image.network(huntImages[index])
      //                     );
      //                 }),
      //               ),
      //             );
      //         }
      //         return Text('No images in Server');
      //     }
      //   }
      // ),
    );
    //return Placeholder();
    // return SingleChildScrollView(
    //   child: Container(
    //     height: 300,
    //     //width: 500,
    //     child: ListView.builder(
    //       scrollDirection: Axis.horizontal,
    //       shrinkWrap: true,
    //       itemCount: huntImages.length,
    //       itemBuilder: (BuildContext context, index){
    //         return Container(
    //           padding: EdgeInsets.all(10.0),
    //           child: Image.network(huntImages[index])
    //         );
    //     }),
    //   ),
    // );
  }
  Widget _buildImageThumb(doc){
    // return FutureBuilder(
    //   future: _dbService.getHuntingImageUrl(doc['thumbName']),
    //   builder: (_, AsyncSnapshot snapshot){
    //     print('got snapshot ${snapshot.data}');
    //     if(snapshot.hasData){
    //       return GestureDetector(
    //         onTap: (){
    //           setState(() {
    //             imageToPreview = doc['imageName'];
    //           });
    //         },
    //         child: Card(elevation: 2.0, child: Image.network(snapshot.data.toString()))
    //       );
    //     }
    //     else
    //      return Text('No image found');
    //   }
    // );
    if(doc != null){
      return GestureDetector(
              onTap: (){
                setState(() {
                  imageToPreview = doc['imageUrl'];
                });
              },
              child: Card(elevation: 2.0, child: Image.network(doc['thumbUrl']))
            );
    }else{
      return Text('No image found');
    }

  }

  Widget _buildImagePreview(){
    // if(imageToPreview != null){
    //   return FutureBuilder(
    //     future: _dbService.getHuntingImageUrl(imageToPreview),
    //     builder: (_, AsyncSnapshot snapshot){
    //       print('got snapshot $snapshot');
    //       if(snapshot.hasData){
    //         return GestureDetector(
    //           onTap: (){
    //             //_buildImagePreview(doc['imageName']);
    //             setState(() {
    //               imageToPreview = null;
    //             });
    //           },
    //           child: Container(
    //             color: Color(0xFF202020),
    //             padding: const EdgeInsets.all(20.0),
    //             child: Center(child: Image.network(snapshot.data.toString())),
    //           )
    //         );
    //       }
    //       else
    //       return Text('No image found');
    //     }
    //   );
    // }else{
    //   return SizedBox.shrink();
    // }

    if(imageToPreview != null){
      return GestureDetector(
        onTap: (){
          //_buildImagePreview(doc['imageName']);
          setState(() {
            imageToPreview = null;
          });
        },
        child: Container(
          color: Color(0xFF202020),
          padding: const EdgeInsets.all(20.0),
          child: Center(child: Image.network(imageToPreview)),
        )
      );
    }else{
      return SizedBox.shrink();
    }
  }

  Widget _buildGuideComments(context){
    return Container(
      child: Text('Guide Comments', style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),),
    );
    //return ListView.builder(itemBuilder: null)
  }
  Widget _buildWaypointsMap(context){
    return Container(
      padding: EdgeInsets.all(10.0),
      child: Card(
        elevation: 2.0,
        color: Colors.black54,
        //color: Color(0xFFCDBDA1),
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Image.asset('assets/location.png'),
        ))
    );
    //return ListView.builder(itemBuilder: null)
  }
}
