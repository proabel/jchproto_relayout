import 'package:flutter/material.dart';
import 'package:jchproto4/services/db-service.dart';
import './../services/auth-service.dart';
import './../providers/app-state-provider.dart';
import 'package:provider/provider.dart';
class LoginPage extends StatefulWidget {
  
  LoginPage({Key key}) : super(key: key);

  @override
  
  _LoginPageState createState() => _LoginPageState();
}


class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    return Container(
       child: Scaffold(
          body: Builder(
            builder: (context) => _buildLoginPage(context),
          )
        )
    );
  }
  // todo auto auth on page startup
  //Shows error on login exceptions
  //prameters Error Message , 'Error' or 'Success' as type 
  Widget _buildSnackBar(String message, String type) {
    print('snack rebuilded');
    Color bgColor;
    if(type == 'Error')
      bgColor = Colors.deepOrange;
    else if(type == 'Success')
      bgColor = Colors.green;
    return  SnackBar(
      content: Text(message),
      backgroundColor: bgColor,
      duration: Duration(seconds: 5),
    );
    
  }

  Widget _buildLoginPage(context){
    final _appStateProvider = Provider.of<AppState>(context, listen:false); 
    final _dbService = DBService();
    final AuthService _auth = AuthService();
    final TextEditingController emailCtlr = TextEditingController();
    final TextEditingController passwordCtlr = TextEditingController();
    
    return Container(
      padding: EdgeInsets.all(20.0),
      color: const Color(0xFFEEE5CC),
      child: Center(
        child:Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text('Joshua Creek Hunt', style: TextStyle(fontSize: 40, fontWeight: FontWeight.bold, color: Colors.green),),
            SizedBox(height: 20),
            Container(
              width: 400,
              height: 300,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Flexible(
                    child: TextField(
                      controller: emailCtlr..text = "lead1@yopmail.com",
                      decoration: InputDecoration(
                        labelText: 'Email'
                      ),
                    )
                  ),
                  SizedBox(height: 20),
                  Flexible(
                    child: TextField(
                      controller: passwordCtlr..text = "lead1@123",
                      //obscureText: true,
                      decoration: InputDecoration(
                        labelText: 'Password'
                      ),
                    )
                  ),
                  SizedBox(height: 20),
                  Flexible(
                    child: RaisedButton(
                      onPressed: () async{
                        // useremail and password is passed as creds
                        // on return we save the data in provider
                        // then we get the company data from the user
                        // we save the company in the provider and then 
                        // login to the lead page
                        if(emailCtlr.text.length > 0 && passwordCtlr.text.length > 0){
                          _auth.signInWithCredentials(email: emailCtlr.text, password: passwordCtlr.text).then((user){
                            _appStateProvider.setUser(user);
                            _dbService.getCompany(user['companyID']).then((company){
                              _appStateProvider.setCompany(company);
                              Navigator.pushReplacementNamed(context, '/lead');
                            }).catchError((e){print('error on get company $e');});
                            
                          }).catchError((e){
                            print('got error $e');
                            Scaffold.of(context).showSnackBar(_buildSnackBar('Something went wrong', 'Error'));
                          });
                        }else{
                            Scaffold.of(context).showSnackBar(_buildSnackBar('Enter email & password', 'Error'));
                        }
                      },
                      child: Text('Login'),
                      color: Colors.green,
                      textColor: Colors.white,
                    )
                  )
                ],
              ),
            )
          ],
        )
      ),
    );
  }
}