
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:jchproto4/services/auth-service.dart';
import 'package:provider/provider.dart';

import './../common/image_capture.dart';
import './../providers/app-state-provider.dart';
import './../services/db-service.dart';
import './../utils/enums.dart';
import './../utils/screen-size-util.dart';


const almostWhite = Color(0xFFFAFAFA);
class LeadPage extends StatefulWidget {
  LeadPage({Key key}) : super(key: key);

  @override
  _LeadPageState createState() => _LeadPageState();
}

class _LeadPageState extends State<LeadPage> {
  final _auth = AuthService();
  final _dbService = DBService();
  String userID;
  //String storagePath = 'images/huntingImages/${widget.userID}/${DateTime.now()}.jpg';

  @override
  void initState() {
    super.initState();
    getUserID();
  }

  void getUserID() async {
    final String id = await _auth.getUserID();
    setState(() {
      userID = id;
    });
  }

  @override
  Widget build(BuildContext context) {
    ScreenSize().init(context);
    return Scaffold(
      appBar: _buildAppBar(context),
      body: _buildLeadScreenLayout(context)
    );
  }

  Widget _buildAppBar(context){
    return AppBar(
      actions: <Widget>[
        Padding(
          padding: const EdgeInsets.only(right:20.0),
          child: IconButton(icon: Icon(Icons.power_settings_new, size: 32, color: almostWhite), onPressed: () async{
            await _auth.signOut();
            Navigator.pushReplacementNamed(context, '/');
          }),
        )
      ],
      centerTitle: false,
      title: Padding(
        padding: const EdgeInsets.only(left:20.0),
        child: const Text('Joshua Creek Hunt Plan', style: TextStyle(fontSize: 32, color: almostWhite), textAlign: TextAlign.start,),
      )
    );
  }

  Widget _buildLeadScreenLayout(context){
    return Container(
      color:const Color(0xFFEEE5CC),
      child: OrientationBuilder(
        builder: (context, orientation){
          //print('orientation changed');
          if(ScreenSize.screenWidth > 1200){
            
            return _buildLargeLayout(context);
          }else if(ScreenSize.screenWidth > 800){
            return _buildSmallLayout(context);
          }else{
            //return _buildSmallLayout(context);
            print('building small layout');
            return _buildSmallLayout(context);
          }
        }
      ),
    );
  }  

  Widget _buildLargeLayout(context){
    return Stack(
      children: [
       
        _buildLargeMainPanel(context),
        
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            _buildActionsPanel(context),
            _buildDatePanel(context),
          ],
        ),
        Consumer<AppState>(
          builder: (context, _appState, child){
            String storagePath = 'images/${_appState.huntDay}/';
            return ImageCapture(storagePath, _appState.availablePlans);
          },
        )
        
      ],
    );
  }

  Widget _buildSmallLayout(context){
    return Stack(
      children: [

        _buildSmallMainPanel(context),
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _buildActionsPanel(context),
            SizedBox(height:20.0),
            _buildDatePanel(context),
          ],
        ),
        Consumer<AppState>(
          builder: (context, _appState, child){
            String storagePath = 'images/${_appState.huntDay}/';
            return ImageCapture(storagePath, _appState.availablePlans);
          },
        )
      ],
    );
  }

  Widget _buildActionsPanel(context){
    return FittedBox(
      child: Container(
        color: const Color(0xFFEEE5CC),
        padding: const EdgeInsets.only(top:15.0, left:20.0),
        child: Card(
          color: almostWhite,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(28.0)
          ),
          elevation: 5.0,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              SizedBox(width:15.0),
              IconButton(
                icon: Icon(Icons.send, color: Theme.of(context).primaryColor,), 
                onPressed: null,
                iconSize: 28.0,
                padding: EdgeInsets.symmetric(vertical: 13.0),
              ),
              SizedBox(width: 5.0,),
              IconButton(
                icon: Icon(Icons.lock_open, color: Theme.of(context).primaryColor,), 
                onPressed: null,
                iconSize: 28.0,
              ),
              SizedBox(width: 5.0,),
              IconButton(
                icon: Icon(Icons.edit, color: Theme.of(context).primaryColor,), 
                onPressed: null,
                iconSize: 28.0,
              ),
              SizedBox(width: 5.0,),
              IconButton(
                icon: Icon(Icons.sync, color: Theme.of(context).primaryColor,), 
                onPressed: null,
                iconSize: 28.0,
              ),
              SizedBox(width: 5.0,),
              IconButton(
                icon: (FaIcon(FontAwesomeIcons.listAlt, color: Theme.of(context).primaryColor,)), 
                onPressed: (){
                  print('got Presed');
                  Navigator.pushReplacementNamed(context, '/reports');
                },
                iconSize: 28.0,
              ),
              SizedBox(width:15.0),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildDatePanel(context){
    DateFormat selectedDateFormat = new DateFormat('yMMMd');
    return Consumer<AppState>(
      builder: (context, appState, child){
        DateTime selectedDate = appState.selectedDate;
        return FittedBox(
          child: Container(
            color:const Color(0xFFEEE5CC),
            padding: EdgeInsets.all(20.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                _buildRainSelector(appState),
                SizedBox(width:20),
                _buildAMPMSelector(appState),
                //isRaining ? FaIcon(FontAwesomeIcons.cloudShowersHeavy, size: 32, color: Colors.lightBlue,) : FaIcon(FontAwesomeIcons.cloudSun, size: 32, color: Colors.deepOrange,),
                SizedBox(width:20),
                GestureDetector(
                  onTap: (){
                    _selectDate(context, appState);
                  },
                  child:Text('${selectedDateFormat.format(selectedDate)}', style: TextStyle(fontSize:32, color: Colors.black54,),),
                  //child: Text('DATe')
                ),
                SizedBox(width: 20),
                _buildNetworkStatus(),
                SizedBox(width:10),
              ],
            ),
          )
        );
      }
    );
  }

  Widget _buildAMPMSelector(appState){
    return GestureDetector(
      onTap: (){
        //setState(() {
          appState.selectedHour == Hour.am ? appState.setHour(Hour.pm) : appState.setHour(Hour.am);
        //});
      },
      child: appState.selectedHour == Hour.am ? Text('AM', style: TextStyle(fontSize:32, color: Colors.black54,)) : Text('PM', style: TextStyle(fontSize:32, color: Colors.black54,))
      //child: Text('PM', style: TextStyle(fontSize:32, color: Colors.black54,))
    );
  }
  Widget _buildRainSelector(appState){
    bool isRaining = appState.isRaining;
    return GestureDetector(
      onTap:(){
        appState.setIsRaining(!isRaining);
      },
      child: appState.isRaining ? FaIcon(FontAwesomeIcons.cloudShowersHeavy, size: 32, color: Colors.lightBlue,) : FaIcon(FontAwesomeIcons.cloudSun, size: 32, color: Colors.deepOrange,),
      //child: FaIcon(FontAwesomeIcons.cloudSun, size: 32, color: Colors.deepOrange,),

    );
  }

  Future<Null> _selectDate(BuildContext context, appState) async {
    DateTime selectedDate = appState.selectedDate;
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: appState.selectedDate,
        firstDate: DateTime(2019),
        lastDate: DateTime(2021));

    if (picked != null && picked != selectedDate)
      appState.setSelectedDate(picked);
      //setState(() {
        //selectedDate = picked;
        //huntDay = company['meta']['shortName'] + shortDate.format(picked).toString();
        //_getHuntPLans();
      //});
  }

  Widget _buildNetworkStatus(){
    return StreamBuilder(
      stream: Stream.periodic(Duration(seconds: 3), (x) async{
        try{
          final result = await InternetAddress.lookup('google.com');
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            return true;
          }
        }on SocketException catch(_){
          return false;
        }
      }),
      builder: (_, resultFuture){
        return FutureBuilder(
          future: resultFuture.data,
          builder: (_, result){
            if(result.data != null && result.data)
              return Icon(Icons.wifi, size: 36, color: Theme.of(context).primaryColor);
            else
              return Icon(Icons.signal_wifi_off, size: 36, color: Colors.grey);
          }
        );
      }
    );
  }

  Widget _buildLargeMainPanel(context){
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.only(top: 100.0, left: 20.0 ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
              ConstrainedBox(
                constraints: BoxConstraints(
                  minHeight:100,
                ),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Flexible(flex: 1, child: _buildAvailableGuides(context)),
                    SizedBox(width:20.0),
                    Flexible(flex: 5, child: _buildAvailableParties(context)),
                  ],
                ),
              ),
            SizedBox(height:20.0),
            ConstrainedBox(
              constraints: BoxConstraints(
                minHeight: 100.0
              ),
              child: _buildAssignedParties(context)
            ),
            SizedBox(height:20.0),
            ConstrainedBox(
              constraints: BoxConstraints(
                minHeight: 100.0
              ),
              child: _buildFields(context)
            ),
          ],
        )
      ),
    );
  }

    Widget _buildSmallMainPanel(context){
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.only(top: 180.0, left: 20.0 ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ConstrainedBox(
                constraints: BoxConstraints(
                  minHeight:100,
                ),
                child: _buildAvailableGuides(context)
              ),
              SizedBox(height:20.0),
            ConstrainedBox(
                constraints: BoxConstraints(
                  minHeight:100,
                ),
                child: _buildAvailableParties(context),
                    
            ),
            SizedBox(height:20.0),
            ConstrainedBox(
              constraints: BoxConstraints(
                minHeight: 100.0
              ),
              child: _buildAssignedParties(context)
            ),
            SizedBox(height:20.0),
            ConstrainedBox(
              constraints: BoxConstraints(
                minHeight: 100.0
              ),
              child: _buildFields(context)
            ),
          ],
        )
      ),
    );
  }

  Widget _buildAvailableGuides(context){
    //final _dbService = DBService();
    
    return Consumer<AppState>(
      builder: (context, appState, child){
        List huntingPlans = [];
        _dbService.getHuntingPlans(appState.huntDay).listen((snapshot) { 
          print('occured $snapshot');
          if(snapshot.data != null){
            //setState(() {
              huntingPlans = snapshot.data['plans'];
            //});
          }
        });
        return Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(left:5, bottom: 10),
              child: Text('Available Guides', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
            ),
            Container(
              child: SingleChildScrollView(
                child: StreamBuilder(
                  stream: _dbService.getGuides(),
                  builder: (context, AsyncSnapshot snapshot) {
                    if(snapshot.hasError) return Text('Could not retreive guides');
                    switch(snapshot.connectionState){
                      case ConnectionState.waiting:
                        return Text('Loading...');
                      default:
                        if(snapshot.data.documents != null){
                          //List 
                          return Container(
                            //height: 300,
                            child: SingleChildScrollView(
                              scrollDirection: Axis.vertical,
                              child: DataTable(
                                //headingRowHeight: 0.0,
                                columnSpacing: 0,
                                horizontalMargin: 0,
                                columns: [DataColumn(
                                  label: _buildGreenHeader('Guide Name'),
                                )], 
                                rows: snapshot.data.documents.where((doc){
                                  bool unAssigned = true;
                                  huntingPlans.forEach((plan) { 
                                    if(plan['guideID'] == doc.documentID)
                                      unAssigned = false;
                                  });
                                  return unAssigned;
                                }).map<DataRow>((doc){
                                  Map guide = doc.data;
                                  ////print('guide doc is ${doc.documentID}');
                                  return  DataRow(
                                    cells: <DataCell>[DataCell(
                                      Container(
                                        child: ListTile(
                                          title: Text(guide['meta']['firstName'], style: TextStyle(fontSize:16)),
                                          trailing: PopupMenuButton(
                                            icon: Icon(Icons.more_vert),
                                            //child: Text('Hello There'),
                                            onSelected: (planIndex) { 
                                              Map<String, dynamic> alteredPlans = {'plans': huntingPlans};
                                              alteredPlans['plans'][planIndex]['guideID'] = doc.documentID;
                                              _dbService.assignGuide(alteredPlans, appState.huntDay);
                                            },
                                            itemBuilder: (BuildContext context){
                                              List<PopupMenuEntry> listOfParties = [];
                                              huntingPlans.asMap().forEach((index, plan){
                                                //print('guideID ${plan["guideID"]}');
                                                if(plan['guideID'].length == 0){
                                                  listOfParties.add(PopupMenuItem(
                                                    value: index,
                                                    child: Text(plan['planID']),
                                                  ));
                                                }
                                              });
                                              return listOfParties;
                                            },
                                          )
                                      )))
                                    ]
                                  );
                                }).toList()
                              ),
                            ),
                          );
                        }else{
                          return Text('No guides available');
                        }
                    }
                  }
                ),
              ),
            ),
          ],
        );
      },
    );
  }

  Widget _buildAvailableParties(context){
    //final _dbService = DBService();
    return Consumer<AppState>(
      builder: (context, appState, child){
        return Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(left:5, bottom: 10),
              child: Text('Hunters - Guide Un-Assigned', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: StreamBuilder(
                stream: _dbService.getHuntingPlans(appState.huntDay),
                initialData: {'data':[]}, 
                builder: (context, AsyncSnapshot snapshot) {
                  ////print('got plan for hunt ${snapshot.data}');
                  if(snapshot.hasError) return Text('Could not retreive hunting parties');
                  switch(snapshot.connectionState){
                    case ConnectionState.waiting:{
                      return Text('Loading');
                    }
                    break;
                    case ConnectionState.active:{
                      print('connection active');
                      if(snapshot.hasData && snapshot.data.exists){
                        //var data = snapshot.data;
                        ////print(snapshot.data['plans']);
                        //List plans = snapshot.data['plans'];
                        print(snapshot.data.exists);
                        print('got plans ${snapshot.data["plans"]}');
                        List plans = snapshot.data["plans"];
                        appState.setAvailablePlans(plans);
                        return DataTable(
                          columnSpacing: 0.0,
                          horizontalMargin: 0.0,
                          columns: <DataColumn>[
                            DataColumn(
                              label: _buildGreenHeader('Party Name')
                            ),
                            DataColumn(
                              label: _buildGreenHeader('Hunters')
                            ),
                            DataColumn(
                              label: _buildGreenHeader('Duck')
                            ),
                            DataColumn(
                              label: _buildGreenHeader('PH')
                            ),
                            DataColumn(
                              label: _buildGreenHeader('Chukar')
                            ),
                            DataColumn(
                              label: _buildGreenHeader('Red Leg')
                            ),
                            DataColumn(
                              label: _buildGreenHeader('Huns')
                            ),
                            DataColumn(
                              label: _buildGreenHeader('Quail')
                            ),
                            DataColumn(
                              label: _buildGreenHeader('Inhouse Notes')
                            ),
                            DataColumn(
                              label: _buildGreenHeader('Guest Notes')
                            )
                          ], 
                          rows: plans.where((plan)=> plan['guideID'].length == 0).map<DataRow>((doc){
                            //Map plan = doc.data;
                            //print('plan $doc');
                            List preys = doc['preys'];
                            return DataRow(cells: <DataCell>[
                              DataCell(_horizontalPaddedText(doc['huntingParty']['huntingPartyName'], 10)),
                              DataCell(_horizontalPaddedText(doc['huntingParty']['hunters'].length.toString(), 20)),
                              DataCell(_horizontalPaddedText(_getPreyNumbers('8CiHC0h65RMc6irA3q90', preys), 20)),
                              DataCell(_horizontalPaddedText(_getPreyNumbers('AV1DFnXCug5AurQcI50V', preys), 20)),
                              DataCell(_horizontalPaddedText(_getPreyNumbers('6fRV22YeNRbDCKopdfJY', preys), 20)),
                              DataCell(_horizontalPaddedText(_getPreyNumbers('wATjeSr3ojkFD5n1c5jW', preys), 20)),
                              DataCell(_horizontalPaddedText(_getPreyNumbers('rlF7VIsm3D3YH0PmQ6u5', preys), 20)),
                              DataCell(_horizontalPaddedText(_getPreyNumbers('IZXClUgzKGx0Mcn33x23', preys), 20)),
                              DataCell(_horizontalPaddedText('Needs frequent breaks',20)),
                              DataCell(_horizontalPaddedText('Color blind', 20)),
                            ]);
                          }).toList()
                        );
                      }else{
                        return Text('No hunting parties on this date');
                      }
                    }
                    break;
                    default:
                      return Text('No hunting parties on this date');
                  }
                }
              ),
            ),
          ],
        );
      }
    );
  }

  Widget _buildAssignedParties(context){
      return Consumer<AppState>(
      builder: (context, appState, child){
        List huntingPlans = [];
        List guides = [];
        _dbService.getHuntingPlans(appState.huntDay).listen((snapshot) { 
          print('occured $snapshot');
          if(snapshot.data != null){
            huntingPlans = snapshot.data['plans'];
          }
        });
        _dbService.getGuides().listen((snapshot) {
          print('guide data $snapshot');
          if(snapshot.documents != null){
            guides = snapshot.documents;
          }
        });
        return Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(left:5, bottom: 10),
              child: Text('Hunters - Guide Un-Assigned', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
            ),
            SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: StreamBuilder(
                    stream: _dbService.getHuntingPlans(appState.huntDay),
                    builder: (context, AsyncSnapshot snapshot) {
                      ////print('got plan for hunt ${snapshot.data}');
                      if(snapshot.hasError) return Text('Could not retreive hunting parties');
                      switch(snapshot.connectionState){
                        case ConnectionState.waiting:
                          return Text('Loading');
                        default:
                        if(snapshot.hasData && snapshot.data.exists){
                          //var data = snapshot.data;
                          print('plan data ${snapshot.data["plans"]}');
                          List plans = snapshot.data['plans'];
                          //List plans = [];
                         return DataTable(
                          columnSpacing: 0.0,
                          horizontalMargin: 0.0,
                          columns: <DataColumn>[
                            DataColumn(
                              label: _buildGreenHeader('Party Name')
                            ),
                            DataColumn(
                              label: _buildGreenHeader('Guide Name')
                            ),
                            DataColumn(
                              label: _buildGreenHeader('Hunters')
                            ),
                            DataColumn(
                              label: _buildGreenHeader('Duck')
                            ),
                            DataColumn(
                              label: _buildGreenHeader('PH')
                            ),
                            DataColumn(
                              label: _buildGreenHeader('Chukar')
                            ),
                            DataColumn(
                              label: _buildGreenHeader('Red Leg')
                            ),
                            DataColumn(
                              label: _buildGreenHeader('Huns')
                            ),
                            DataColumn(
                              label: _buildGreenHeader('Quail')
                            ),
                            DataColumn(
                              label: _buildGreenHeader('Inhouse Notes')
                            ),
                            DataColumn(
                              label: _buildGreenHeader('Guest Notes')
                            ),
                            DataColumn(
                              label: _buildGreenHeader('')
                            )
                          ], 
                          rows: plans.where((plan)=> plan['guideID'].length > 0).map<DataRow>((doc){
                            //Map plan = doc.data;
                            //print('plan $doc');
                            List preys = doc['preys'];
                            return DataRow(cells: <DataCell>[
                              DataCell(_horizontalPaddedText(doc['huntingParty']['huntingPartyName'], 20)),
                              DataCell( _horizontalPaddedText(_getGuideName(doc['guideID'], guides), 20),),
                              DataCell(_horizontalPaddedText(doc['huntingParty']['hunters'].length.toString(), 20)),
                              
                              DataCell(_horizontalPaddedText(_getPreyNumbers('8CiHC0h65RMc6irA3q90', preys), 20)),
                              DataCell(_horizontalPaddedText(_getPreyNumbers('AV1DFnXCug5AurQcI50V', preys), 20)),
                              DataCell(_horizontalPaddedText(_getPreyNumbers('6fRV22YeNRbDCKopdfJY', preys), 20)),
                              DataCell(_horizontalPaddedText(_getPreyNumbers('wATjeSr3ojkFD5n1c5jW', preys), 20)),
                              DataCell(_horizontalPaddedText(_getPreyNumbers('rlF7VIsm3D3YH0PmQ6u5', preys), 20)),
                              DataCell(_horizontalPaddedText(_getPreyNumbers('IZXClUgzKGx0Mcn33x23', preys), 20)),
                              DataCell(_horizontalPaddedText('Needs frequent breaks',20)),
                              DataCell(_horizontalPaddedText('Color blind', 20)),
                              DataCell( PopupMenuButton(
                                      icon: Icon(Icons.more_vert),
                                      //child: Text('Hello There'),
                                      onSelected: (planID) { 
                                        int planIndex;
                                        huntingPlans.asMap().forEach((index, plan) { 
                                          if(plan['planID'] == planID)
                                            planIndex = index;
                                        });
                                        Map<String, dynamic> alteredPlans = {'plans': huntingPlans};
                                        alteredPlans['plans'][planIndex]['guideID'] = "";
                                        _dbService.assignGuide(alteredPlans, appState.huntDay);
                                      },
                                      itemBuilder: (BuildContext context)=>[
                                        PopupMenuItem(
                                          value: doc['planID'],
                                          child: Text('Unassign'),
                                        )
                                      ]
                                  ),),
                            ]);
                          }).toList()

                          );
                        }else{
                          return Text('No hunting parties on this date');
                        }
                      }
                      
                    }
                  ),
                ),
              ),
          ],
        );
      }
    );
  }

  Widget _buildFields(context){
    return Container(
      //padding: EdgeInsets.all(10.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(left: 5),
            child: Text('Field Assignment', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold), textAlign: TextAlign.start,),
          ),
          StreamBuilder(
            stream: _dbService.getFields(),
            builder: (_, AsyncSnapshot snapshot){
              if(snapshot.hasError) return Text('Couldnt retreive Fields');
              switch(snapshot.connectionState){
                case ConnectionState.waiting:
                  return Text('Loading...');
                break;
                case ConnectionState.active: {
                  if(snapshot.hasData && snapshot.data != null){
                    return SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: Row(
                        children: snapshot.data.documents.map<Widget>((field){
                          return Container(
                            padding: EdgeInsets.only(right:10.0, top:10.0),
                            child: Card(
                              color: Color(0xFFCDBDA1),
                              child: Padding(
                                padding: const EdgeInsets.symmetric(horizontal:30.0, vertical: 40.0),
                                child: Text(field['name'].toUpperCase(), style: TextStyle(fontSize: 18.0,fontWeight: FontWeight.bold, color: Colors.black)),
                              )
                            )
                          );
                        }).toList(),
                      ),
                    );
                  }else{
                    return Text('No fields available');
                  }
                }
                break;
                default:
                  return Text('No fields available');
              }
            }
          )
        ],
      )
    );
  }

  String _getPreyNumbers(String preyID, List preys){
    final int index = preys.indexWhere((prey) => prey['preyID'] == preyID);
    final String numbers = preys[index]['numbers'].toString();
    return numbers != null ? numbers : '0'; 
  }

  String _getGuideName(String guideID, guides){
    String name = "No Name";
    guides.forEach((guide) {
      if(guide.documentID == guideID){
        name = guide.data['meta']['firstName'];
      }
    });
    return name;
  }
  Widget _horizontalPaddedText(String text, double padding){
    return Padding(
      padding:  EdgeInsets.symmetric(horizontal:padding),
      child: Center(child: Text(text, style: TextStyle(fontSize: 16), )),
    );
  }
  Widget _buildGreenHeader(String label){
    return Expanded(
      child: Container(
        padding:EdgeInsets.symmetric(horizontal:20),
        height: double.infinity,
        color:Theme.of(context).primaryColor,
        //color: Color(0xFF22F11),
        child: Align(child: Text(label, style: TextStyle(color:almostWhite, fontWeight: FontWeight.bold, fontSize: 16),), alignment: Alignment.centerLeft,)
      ),
    );
  }
}
