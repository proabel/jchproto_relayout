import 'package:flutter/material.dart';
import 'package:jchproto4/services/auth-service.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({Key key}) : super(key: key);

  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  AuthService _auth = AuthService();
  @override
  void initState() { 
    super.initState();
    //check for auth
    checkAuth();  
  }
  void checkAuth() async{
    if(await _auth.isSignedIn()){
      //Navigator.pushReplacementNamed(context, '/lead');
      //todo need to get logic from login page to splash page
      //so redirecting to login page even on auth success
      Navigator.pushReplacementNamed(context, '/login');
    }else{
      Navigator.pushReplacementNamed(context, '/login');
    }
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      child: CircularProgressIndicator(),
    );
  }
}