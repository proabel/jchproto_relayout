
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import './pages/login-page.dart';
import './pages/lead-page.dart';
import './pages/report-page.dart';
import './providers/app-state-provider.dart';
import 'pages/splash-page.dart';
void main() {
  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => AppState())
      ],
      child: MyApp(),
    )
    
  );
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'JCHProto',
      theme: ThemeData(
        
        primarySwatch: Colors.green,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      routes: {
        '/' : (context) => SplashPage(),
        '/login' : (context) => LoginPage(),
        '/lead' : (context) => LeadPage(),
        '/reports' : (context) => ReportPage()
      },
    );
  }
}
