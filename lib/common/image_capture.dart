import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'dart:io';
import 'package:image_picker/image_picker.dart';
import 'package:jchproto4/services/auth-service.dart';
import 'package:jchproto4/services/db-service.dart';
//import 'package:firebase/firestore.dart';
class ImageCapture extends StatefulWidget {
  final String path;
  final List plans;
  ImageCapture(this.path, this.plans);

  @override
  _ImageCaptureState createState() => _ImageCaptureState();
}

class _ImageCaptureState extends State<ImageCapture> {
  //File _capturedImage;
  final DBService _dbService = DBService();
  //final FirebaseStorage _storage = FirebaseStorage(storageBucket:'gs://hunt-planning-app.appspot.com');
  StorageUploadTask _storageUploadTask;

  void startUpload(fileToUpload){
    print('starting upload');
    String filename = '${DateTime.now()}.jpg';
    String filePath = '${widget.path}$filename';
    //_storage.ref().child(storagePath).putFile(fileToUpload);
    //setState(() async{
      //_storageUploadTask = _storage.ref().child(filePath).putFile(fileToUpload);
    _dbService.uploadAndStoreImage(filePath, filename, fileToUpload, widget.plans);
    //});
  }


  Future<void> _clickImage() async{
    File clickedImage = await ImagePicker.pickImage(source: ImageSource.camera);

    startUpload(clickedImage);
    //ImageUpload(file: clickedImage);
    // setState(() {
    //   _capturedImage = clickedImage;
    // });
  }
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      //crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [  
            FloatingActionButton(
              backgroundColor: Colors.deepOrange,
              onPressed: (){
                _clickImage();
              },
              child: Icon(Icons.camera_alt),
            ),
            SizedBox(width: 20.0,),
          ],
        ),
        //SizedBox(height: 20.0,),
        Container(
          height: 20.0,
          child: _buildUploader(), 
        )
      ],
    );
  }
  Widget _buildUploader(){
    if(_storageUploadTask != null){
      return StreamBuilder<StorageTaskEvent>(
            stream: _storageUploadTask.events,
            builder: (_, snapshot){
              print('started uploading');
              var event = snapshot?.data?.snapshot;
              double progressPercent = event != null
                    ? event.bytesTransferred / event.totalByteCount
                    : 0;
              if(_storageUploadTask.isInProgress)
                return Container(
                  color:Colors.deepOrange, 
                  child: Text('uploading picture', style: TextStyle(color: Colors.white),)
                );
      
              return SizedBox.shrink();
              //else if(_storageUploadTask.)
              // return Container(
              //   child: _storageUploadTask.isComplete ? SizedBox.shrink() : 
                
              // );
            },
          );
    }
    return SizedBox.shrink();
  }
}