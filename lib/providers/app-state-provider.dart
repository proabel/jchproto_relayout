import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import './../utils/enums.dart';
class AppState extends ChangeNotifier{
  Map _currentUser;
  Map _company;
  DateTime _selectedDate = DateTime.now();
  Hour _selectedHour = Hour.am;
  bool _isRaining = false;
  String _huntDay;
  List _availableGuides;
  List _availablePlans;
  List _huntPictures;

  Map get currentUser => _currentUser;
  Map get company => _company;
  DateTime get selectedDate => _selectedDate;
  Hour get selectedHour => _selectedHour;
  bool get isRaining => _isRaining;
  String get huntDay => _huntDay;
  List get availableGuides => _availableGuides;
  List get availableParties => _availablePlans;
  List get availablePlans => _availablePlans;
  List get huntPictures => _huntPictures;

  void setUser(Map user){
    _currentUser = user;
    notifyListeners();
  }

  void setCompany(Map company){
    _company = company;
    setHunDay(_selectedDate, _company);
    notifyListeners();
  }

  void setSelectedDate(DateTime dt){
    _selectedDate = dt;
    setHunDay(_selectedDate, _company);
    notifyListeners();
  }

  void setHour(Hour hour){
    _selectedHour = hour;
    notifyListeners();
  }

  void setIsRaining(bool value){
    _isRaining = value;
    notifyListeners();
  }

  void setHunDay(DateTime date, Map company){
    //DateFormat shortDate = new DateFormat('ddMMy');
    //_huntDay = company['meta']['shortName'] + shortDate.format(selectedDate).toString();
    _huntDay = "JCH06062020";
    notifyListeners();
  }

  void setAvailableGuides(List guides){
    _availableGuides = guides;
    notifyListeners();
  }

  void setPictures(List pictures){
    _huntPictures = pictures;
    notifyListeners();
  }

  void setAvailablePlans(List plans){
    _availablePlans = plans;
    notifyListeners();
  }
}